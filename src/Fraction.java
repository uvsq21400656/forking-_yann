public class Fraction {
	
	private int num;
	private int deno;
	
	Fraction(int num,int deno)
	{
		this.num = num;
		this.deno = deno;
	}
	
	Fraction(int num)
	{
		this.num = num;
		this.deno = 1;
	}
	
	Fraction()
	{
		this.num = 0;
		this.deno = 1;
	}
	public static final Fraction ONE = new Fraction(1);
	
	public static final Fraction ZERO = new Fraction();
	
	public int getNum() {
		return num;
	}


	public int getDeno() {
		return deno;
	}
	
	public String toString()
	{
		return this.num + "/" + this.deno;
	}
	
	public String calcule()
	{
		double x = (double) this.num/(double) this.deno;
		return "Resultat = " + x;
	}
	
	public String change2()
	{
		return "change";
	}
	
	public String Carrotte()
	{
		return " Mmmmmm les carottes c mieux que le riz et le choux";
	}
	}
}
